<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- 
  hashCode Systems.
  
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 
 -->	
<html>
<head>
<link rel="icon" type="image/png" href="./images/favicon.png" />
<title>IWMI Resource Viewer</title>
<link rel="stylesheet" href="style.css" />
</head>
<body>
	<div id="wrapper">
		<%@include file='/jsp/mainLayout/header.jsp'%>
		<div id="search-seperator"></div>
		<div id="content">
			<div id="grid-data-container">
				<%@include file='/jsp/layout/maindatalst-view_body.jsp'%></div>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>