<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- 
  hashCode Systems.
  
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 
 -->
<html>
<head>
<link rel="icon" type="image/png" href="./images/favicon.png" />
<title>IWMI Resource Viewer</title>
<link rel="stylesheet" href="style.css" />
<script type="text/javascript">
function newPopup(url) {
    var left = (screen.width/2)-(960/2);
    var top = (screen.height/2)-(600/2);

	popupWindow = window.open(
		url,'popUpWindow','height=600,width=960,left='+left+',top='+top+',resizable=no,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=no')

}
</script>

</head>
<body>
	<div id="wrapper">    	
		<%@include file='/jsp/mainLayout/header.jsp'%>
		<div id="search-seperator"></div>
		<div id="content">
			<div class="main-image-container">
				<img src="images/main-image.jpg" width="528px" height="360px" />
                <div align="center">
                	<a href="http://www.iwmi.cgiar.org" target="_blank"><img src="./images/cgiar-web.png"/></a>
					<a href="jsp/about-iwmi.html" class='ajax'><img style="margin-left:30px;" src="./images/about-iwmi.png"/></a>
                	<a href="http://wle.cgiar.org/blogs/" target="_blank"><img style="margin-left:30px;" src="./images/ageco.png"/></a>
                </div>
				<div id="footer-logo">
					<img src="images/footer_logo.jpg" height="93px"/><img src="images/cgiar_logo.jpg" />
				</div>   
				<div id="slogan-wrapper" align="center">                 	
					<div id="slogan">Water for a food-secure world</div>                   
					<div id="dot-image">
						<img src="images/dots.jpg" />
					</div>
				</div>             
			</div>
			<div class="main-vertical-seperator">&nbsp;</div>
			<div class="main-data-container">
				<div id="main-title-usb">On this USB</div>
				<p style="margin-bottom:5px; font-weight:bold;"> Click here for :</p>
				<ol style="margin:0px 0px 0px 0px;">
                	<li><a href="${applicationScope.properties.publications}" target="_blank">Publications</a></li>
                    <li onclick="invokeLink('Water Data')" class="home-link">Water Data</li>                  
                    <li onclick="invokeLink('Research Project')" class="home-link">Research Projects</li>
                    <li>Communications Resources
                        <ol type="a">
                            <li onclick="invokeLink('Photos')" class="home-link">Photos</li>
                            <li onclick="invokeLink('Video')" class="home-link">Videos</li>
                            <li onclick="invokeLink('Audio')" class="home-link">Audios</li>
                            <li onclick="invokeLink('Power Points')" class="home-link">Powerpoints</li>
                            <li onclick="invokeLink('Maps')" class="home-link">Maps</li>
                            <li onclick="invokeLink('Posters')" class="home-link">Posters</li>
                            <li onclick="invokeLink('Success Stories')" class="home-link">Success Stories</li>
                            <li onclick="invokeLink('Media')" class="home-link">Media</li>
                        </ol>
                    </li>
     			 </ol>
				<div id="update-wrapper"><div class="download-caption">Connect your computer to the internet and press download button to download latest resources to USB</div><br/><div id="update-btn"><a href="${applicationScope.properties.syncclient}" class='iframe'><img src="images/download.png"></a>
                               </div>                               
				<div style="clear: both;"></div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		function invokeLink(selection)
		{
			document.getElementById("resource-type").value = selection;
			searchCall();
		}
		
	</script>
</body>
</html>