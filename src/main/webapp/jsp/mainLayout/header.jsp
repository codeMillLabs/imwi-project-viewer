<%-- 
  hashCode Systems.
  
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
  --%>
<div id="search-bar">
	<div id="search-buttons">
		<form id="searchForm" action="iwmiDataView" method="post">
			<div class="header-items">
            <div style="float: left">
					<label class="label-style">Keyword : </label><input type="text"
						name="st"
						<c:choose> 
						<c:when test="${requestScope.st == '' || requestScope.st == null } "> value="Search..." </c:when>
						<c:otherwise>value="${requestScope.st}"</c:otherwise></c:choose>
						id="search-text" onclick="removeText(this)" />
			 </div>
             </div>
             <div class="clear"></div>   
             <div class="header-items">  
            <label class="label-style">Resource Type :</label> <select id="resource-type" name="resourceType">
				<option value="-">All</option>
				<option value="Water Data"
					<c:if test="${requestScope.sd == 'Water Data'}"> selected="selected" </c:if>>Water Data</option>
				<option value="Research Project"
					<c:if test="${requestScope.sd == 'Research Project'}"> selected="selected" </c:if>> Research Projects</option>
				<option disabled="disabled" style="color:#000">Communications Resources</option>
					<option value="Photos" style="padding-left:10px"
						<c:if test="${requestScope.sd == 'Photos'}"> selected="selected" </c:if>> Photos</option>
					<option value="Video" style="padding-left:10px"
						<c:if test="${requestScope.sd == 'Video'}"> selected="selected" </c:if>> Videos</option>
					<option value="Audio" style="padding-left:10px"
						<c:if test="${requestScope.sd == 'Audio'}"> selected="selected" </c:if>> Audios</option>
					<option value="Power Points" style="padding-left:10px"
						<c:if test="${requestScope.sd == 'Power Points'}"> selected="selected" </c:if>>Power
						Points</option>
					<option value="Maps" style="padding-left:10px"
						<c:if test="${requestScope.sd == 'Maps'}"> selected="selected" </c:if>> Maps</option>
					<option value="Posters" style="padding-left:10px"
						<c:if test="${requestScope.sd == 'Posters'}"> selected="selected" </c:if>> Posters</option>
					<option value="Success Stories" style="padding-left:10px"
						<c:if test="${requestScope.sd == 'Success Stories'}"> selected="selected" </c:if>>Success
						Stories</option>
					<option value="Media" style="padding-left:10px"
						<c:if test="${requestScope.sd == 'Media'}"> selected="selected" </c:if>> Media</option>
			</select>
             <input type="button" name="Search" id="search-btn" value="Search"
				onclick="searchCall()" style="cursor: pointer;" />

              <input type="reset" name="Reset"
				id="reset-btn"  onclick="resetForm()" style="cursor: pointer;"/>
				
				<a href="jsp/help-support.html" class='ajax'><div id="help-btn"></div></a>
             </div>

			<!-- Hidden field -->
			<input type="hidden" id="ct-1" name="ct" value="${requestScope.ct}" />
			<!-- <input type="hidden" id="srt-1" name="srt" value="${requestScope.srt}" />  -->
			<input type="hidden" id="sd-1" name="sd" value="${requestScope.sd}" />
		</form>
	</div>
    <div class="clear"></div>
</div>
<!-- overlayed element -->
<div class="apple_overlay" id="overlay">
  <!-- the external content is loaded inside this tag -->
  <div class="contentWrap"></div>
</div>
<script type="text/javascript">
	function searchCall() {
		var searchFld = document.getElementById("search-text");
		if (searchFld.value == "Search...")
			searchFld.value = "";

		var ctHiddenFld = document.getElementById("ct-1");
		var sdHiddentFld = document.getElementById("sd-1");
		var resourceTypeVal = document.getElementById("resource-type").value;

		switch (resourceTypeVal) {
		case "Water Data":
			ctHiddenFld.value = "content_type:datasets";
			sdHiddentFld.value = "Water Data";
			break;
		case "Research Project":
			ctHiddenFld.value = "content_type:project";
			sdHiddentFld.value = "Research Project";
			break;
		case "Photos":
			ctHiddenFld.value = "content_type:1";
			sdHiddentFld.value = "Photos";
			break;
		case "Video":
			ctHiddenFld.value = "content_type:3";
			sdHiddentFld.value = "Video";
			break;
		case "Audio":
			ctHiddenFld.value = "content_type:4";
			sdHiddentFld.value = "Audio";
			break;
		case "Power Points":
			ctHiddenFld.value = "content_type:5";
			sdHiddentFld.value = "Power Points";
			break;
		case "Maps":
			ctHiddenFld.value = "content_type:6";
			sdHiddentFld.value = "Maps";
			break;
		case "Posters":
			ctHiddenFld.value = "content_type:7";
			sdHiddentFld.value = "Posters";
			break;
		case "Success Stories":
			ctHiddenFld.value = "content_type:8";
			sdHiddentFld.value = "Success Stories";
			break;
		case "Media":
			ctHiddenFld.value = "content_type:11";
			sdHiddentFld.value = "Media";
			break;
		default:
			ctHiddenFld.value = "";
			sdHiddentFld.value = "";
			break;
		}

		var form = document.getElementById("searchForm");
		form.submit();
	}

	function removeText(field) {
		if (field.value == "Search...")
			field.value = "";
	}
	
	function resetForm()
	{
		document.getElementById("search-text").value = "Search...";
		document.getElementById("resource-type").value = "";
	}
</script>
	<script src="js/jquery.min.js"></script>
	<script src="js/jquery.colorbox.js"></script>
	<script>
		$(document).ready(function(){
			//Examples of how to assign the ColorBox event to elements
			$(".ajax").colorbox();
			$(".iframe").colorbox({iframe:true, width:"45%", height:"55%"});
		});
	</script>
