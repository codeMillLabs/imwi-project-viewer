<%-- 
  hashCode Systems.
  
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 
 --%>

<%@page import="com.hashcode.iwmi.wds.utils.ResourceType"%>
<%@page import="java.util.Collection"%>
<%@page import="org.apache.solr.common.SolrDocument"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.hashcode.iwmi.wds.utils.IWMIUtils"%>
<%@page import="org.apache.solr.common.SolrDocumentList"%>
<%@page import="com.hashcode.iwmi.wds.utils.IWMIConst"%>

<script type="text/javascript">
	function searchCategory() {
		var form = document.getElementById("categorySearchFrm");
		form.submit();
	}
</script>

<div>
	<div id="details-view">
		<div class="mainTopic">
			<c:if test="${requestScope.sd != null}">${requestScope.sd}</c:if>
		</div>
		<div class="inner-select">
			<c:if test="${requestScope.no_categs > 0}">
				<%
					Collection<String> categs = (Collection<String>) request
								.getAttribute(IWMIConst.CATEGORY_LST.getDesc());
						Iterator<String> categItr = categs.iterator();
						String catSelected = (String) request
								.getAttribute(IWMIConst.CATEGORY_SEARCH.getDesc());
				%>
				<form id="categorySearchFrm" action="iwmiDataView" method="post">

					<select id="categoryTypeSel" name="cat" onchange="searchCategory()">
						<option value="">All</option>
						<%
							while (categItr.hasNext()) {
									String categ = categItr.next();
									String categDspVal = (categ.equals("1900")) ? "Other" : categ;
						%>
						<option value="<%=categ%>"
							<%if (categ.equals(catSelected)) {%>
							selected="selected" <%}%>><%=categDspVal%></option>
						<%
							}
						%>
					</select>

					<!-- Hidden field -->
					<input type="hidden" id="ctview-1" name="ct"
						value="${requestScope.ct}" /> <input type="hidden" id="srtview-1"
						name="srt" value="${requestScope.srt}" /> <input type="hidden"
						id="sdview-1" name="sd" value="${requestScope.sd}" />
					<input type="hidden" id="st-ref" name="st-ref"
						value="${requestScope.st}" />
				</form>
			</c:if>

		</div>
		<div class="sort-order">
			Sort Order :
			<c:url value="/iwmiDataView" var="titleSort">
				<c:param name="ct" value="${requestScope.ct}" />
				<c:param name="st" value="${requestScope.st}" />
				<c:param name="srt" value="${requestScope.srt}" />
				<c:param name="srtb" value="title" />
				<c:param name="sp" value="0" />
				<c:param name="sd" value="${requestScope.sd}" />
				<c:param name="sa" value="${requestScope.sa}" />
			</c:url>
			<c:url value="/iwmiDataView" var="regionSort">
				<c:param name="ct" value="${requestScope.ct}" />
				<c:param name="st" value="${requestScope.st}" />
				<c:param name="srt" value="${requestScope.srt}" />
				<c:param name="srtb" value="location" />
				<c:param name="sp" value="0" />
				<c:param name="sd" value="${requestScope.sd}" />
				<c:param name="sa" value="${requestScope.sa}" />
			</c:url>
			<c:url value="/iwmiDataView" var="authorSort">
				<c:param name="ct" value="${requestScope.ct}" />
				<c:param name="st" value="${requestScope.st}" />
				<c:param name="srt" value="${requestScope.srt}" />
				<c:param name="srtb" value="authors" />
				<c:param name="sp" value="0" />
				<c:param name="sd" value="${requestScope.sd}" />
				<c:param name="sa" value="${requestScope.sa}" />
			</c:url>

			<a href='<c:out value="${titleSort}"/>'> <c:choose>
					<c:when test='${requestScope.srtb == "title" }'>
						<b>Title</b>
					</c:when>
					<c:otherwise>
			       Title
			    </c:otherwise>
				</c:choose></a>&nbsp;|&nbsp; <a href='<c:out value="${regionSort}"/>'> <c:choose>
					<c:when test='${requestScope.srtb == "location" }'>
						<b>Region/Location</b>
					</c:when>
					<c:otherwise>
			       Region/Location
			    </c:otherwise>
				</c:choose></a>&nbsp;
			<!-- <a href='<c:out value="${authorSort}"/>'>Author/Presenter</a>  -->
		</div>
	</div>
	<div class="clear"></div>
	<div class="grid-note">
		<c:if test="${requestScope.title_note != null}">${requestScope.title_note}</c:if>
	</div>
	<div align="top">
		<c:choose>
			<c:when test="${requestScope.no_data == null}">
				<%
					long startPos = (Long) request
									.getAttribute(IWMIConst.START_POINT.getDesc());
							long totalRecs = (Long) request
									.getAttribute(IWMIConst.TOTAL_NO_REC.getDesc());

							Map<String, List<SolrDocument>> dataMap = (Map<String, List<SolrDocument>>) request
									.getAttribute(IWMIConst.DATA_SET.getDesc());
							ResourceType resourceType = (ResourceType) request
									.getAttribute(IWMIConst.RESOURCE_TYPE.getDesc());
							Collection<String> keys = (Collection<String>) request
									.getAttribute(IWMIConst.DATA_SET_KEY_ORDER
											.getDesc());
							Iterator<String> keyItr = keys.iterator();
				%>
				<div style="min-height: 400px">
					<table class="list-links">
						<tbody>
							<!-- view content goes here -->
							<%
								int c = 0;
										while (keyItr.hasNext()) {
											c++;
											String className = "";
											if (c % 2 == 1) {
												className = "listTableOdd";
											} else {
												className = "listTableEven";
											}
							%>
							<tr valign="top">
								<%
									String grpHeader = keyItr.next();
            						String categName = ("1900".equals(grpHeader)) ? "Other" : grpHeader;
								%>
								<td style="font-weight: bold;" colspan="1"><%=categName%></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<%
								List<SolrDocument> solrDocs = dataMap.get(grpHeader);
											for (SolrDocument doc : solrDocs) {
							%>
							<tr class="<%=className%>">
								<td></td>
								<td colspan="2"><c:url value="/imwiDetailView"
										var="itemUrl">
										<c:param name="ct"
											value='<%="id:" + doc.getFieldValue("id")%>' />
									</c:url> <a href='<c:out value="${itemUrl}"/>'
									title="<%=doc.getFieldValue("title")%>"> <%=IWMIUtils.getListViewTitle(doc,
									resourceType)%>
								</a></td>
								<td></td>
							<tr>
								<%
									}
								%>
								<%
									}
								%>
							</tr>
						</tbody>
					</table>
				</div>
				<table
					style="font-size: 12px; font-weight: bold; margin: 0 auto; width: 960px;">
					<tbody>
						<tr>
							<!-- Pagination row -->
							<td colspan="4" align="right"><c:if
									test="${requestScope.hasPrevPg == true}">
									<c:url value="/iwmiDataView" var="prevPgUrl">
										<c:param name="ct" value="${requestScope.ct}" />
										<c:param name="st" value="${requestScope.st}" />
										<c:param name="srt" value="${requestScope.srt}" />
										<c:param name="srtb" value="${requestScope.srtb}" />
										<c:param name="sp" value="${requestScope.prevPg}" />
										<c:param name="sd" value="${requestScope.sd}" />
									</c:url>
									<a class="pagination-buttons"
										href='<c:out value="${prevPgUrl}"/>'>&lt;&lt;Prev</a>
								</c:if> &nbsp;Page&nbsp;<c:out value="${requestScope.currentPg}" />&nbsp;of&nbsp;
								<c:out value="${requestScope.totalPg}" /> &nbsp; <c:if
									test="${requestScope.hasNextPg == true}">
									<c:url value="/iwmiDataView" var="nextPgUrl">
										<c:param name="ct" value="${requestScope.ct}" />
										<c:param name="st" value="${requestScope.st}" />
										<c:param name="srt" value="${requestScope.srt}" />
										<c:param name="srtb" value="${requestScope.srtb}" />
										<c:param name="sp" value="${requestScope.nextPg}" />
										<c:param name="sd" value="${requestScope.sd}" />
									</c:url>
									<a class="pagination-buttons"
										href='<c:out value="${nextPgUrl}"/>'>Next&gt;&gt;</a>
								</c:if> <c:url value="/iwmiDataView" var="showAllUrl">
									<c:param name="ct" value="${requestScope.ct}" />
									<c:param name="st" value="${requestScope.st}" />
									<c:param name="srt" value="${requestScope.srt}" />
									<c:param name="srtb" value="${requestScope.srtb}" />
									<c:param name="sp" value="0" />
									<c:param name="sd" value="${requestScope.sd}" />
									<c:param name="sa" value="t" />
								</c:url> <c:if test="${requestScope.sa != 't'}">
									<a class="pagination-buttons"
										href='<c:out value="${showAllUrl}"/>'>&nbsp;&nbsp;Show
										All&nbsp;&nbsp;</a>
								</c:if></td>
							<td><div id="backHomeStyle">
									<div class="backButton">
										<a href="index.jsp">Home</a>
									</div>
								</div></td>
						</tr>
					</tbody>
				</table>
			</c:when>
			<c:otherwise>
				<div class="noRecords">No Records Found</div>
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<div id="backHomeStyle">
					<div class="backButton">
						<a href="JavaScript:void(o);" onclick="history.go(-1)">Back</a>
					</div>
				</div>
			</c:otherwise>

		</c:choose>
	</div>
</div>