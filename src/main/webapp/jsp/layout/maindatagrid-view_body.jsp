<%-- 
  hashCode Systems.
  
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 
 --%>

<%@page import="org.apache.solr.common.SolrDocument"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="com.hashcode.iwmi.wds.utils.IWMIUtils"%>
<%@page import="org.apache.solr.common.SolrDocumentList"%>
<%@page import="com.hashcode.iwmi.wds.utils.IWMIConst"%>
<%@page import="com.hashcode.iwmi.wds.utils.ResourceType"%>

<div>
	<div id="details-view">
		<div class="mainTopic">
			<c:if test="${requestScope.sd != null}">${requestScope.sd}</c:if>
		</div>
		<div class="sort-order">
			Sort Order :
			<c:url value="/iwmiDataView" var="titleSort">
				<c:param name="ct" value="${requestScope.ct}" />
				<c:param name="st" value="${requestScope.st}" />
				<c:param name="srt" value="${requestScope.srt}" />
				<c:param name="srtb" value="title" />
				<c:param name="sp" value="0" />
				<c:param name="sd" value="${requestScope.sd}" />
				<c:param name="sa" value="${requestScope.sa}" />
			</c:url>
			<c:url value="/iwmiDataView" var="regionSort">
				<c:param name="ct" value="${requestScope.ct}" />
				<c:param name="st" value="${requestScope.st}" />
				<c:param name="srt" value="${requestScope.srt}" />
				<c:param name="srtb" value="location" />
				<c:param name="sp" value="0" />
				<c:param name="sd" value="${requestScope.sd}" />
				<c:param name="sa" value="${requestScope.sa}" />
			</c:url>
			<c:url value="/iwmiDataView" var="authorSort">
				<c:param name="ct" value="${requestScope.ct}" />
				<c:param name="st" value="${requestScope.st}" />
				<c:param name="srt" value="${requestScope.srt}" />
				<c:param name="srtb" value="authors" />
				<c:param name="sp" value="0" />
				<c:param name="sd" value="${requestScope.sd}" />
				<c:param name="sa" value="${requestScope.sa}" />
			</c:url>

			<a href='<c:out value="${titleSort}"/>'> <c:choose>
					<c:when test='${requestScope.srtb == "title" }'>
						<b>Title</b>
					</c:when>
					<c:otherwise>
			       Title
			    </c:otherwise>
				</c:choose></a>&nbsp;|&nbsp; <a href='<c:out value="${regionSort}"/>'> <c:choose>
					<c:when test='${requestScope.srtb == "location" }'>
						<b>Region/Location</b>
					</c:when>
					<c:otherwise>
			       Region/Location
			    </c:otherwise>
				</c:choose></a>&nbsp;
			<!-- <a href='<c:out value="${authorSort}"/>'>Author/Presenter</a>  -->
		</div>
	</div>
    <div class="clear"></div>
    <div class="grid-note"><c:if test="${requestScope.title_note != null}">${requestScope.title_note}</c:if></div>
	<div align="top">
		<c:choose>
			<c:when test="${requestScope.no_data == null}">
				<%
					long startPos = (Long) request
									.getAttribute(IWMIConst.START_POINT.getDesc());
							long totalRecs = (Long) request
									.getAttribute(IWMIConst.TOTAL_NO_REC.getDesc());
							SolrDocumentList docList = (SolrDocumentList) request
									.getAttribute(IWMIConst.DATA_SET.getDesc());
							Iterator<SolrDocument> docIterator = docList.iterator();
							Map<ResourceType, String> iconUrlMap = 
						    		  (Map<ResourceType, String>)request.getAttribute(IWMIConst.SHORTCUT_ICONS.getDesc());
				%>
                <div style="min-height: 480px">
				<table style="font-size: 12px;">
					<tbody>
						<!-- view content goes here -->
						<c:forEach begin="1" end="${requestScope.datagrid_row_count}">
							<tr valign="top">
								<c:forEach begin="1" end="${requestScope.datagrid_col_count}">
									<c:if test="<%=docIterator.hasNext()%>">
										<%
											SolrDocument doc = docIterator.next();
										%>
										<td>
											<div style="min-height: 170px; width: 230px;">
												<div class="gridImageWrapper">
												<div class="image-container">
													<div><img src='<%=doc.getFieldValue("thumb_url")%>'
														alt="" onerror="this.onerror=null;this.src='images/default-image.jpg'" height="98px" width="130px" /></div>
											         <c:if test="${requestScope.sd == ''}">
										                 <%
										                 String contentType = (String)doc.getFieldValue("content_type");
										                 ResourceType type = IWMIUtils.getResourceTypeFromData(contentType);
                 							             String overlayIconUrl = iconUrlMap.get(type);
										                 %>
										                  
														 <div class="image-container-thumnail-icon">
														   <img src='<%=iconUrlMap.get(IWMIUtils.getResourceTypeFromData((String)doc.getFieldValue("content_type")))%>'
														    alt="" width="40px"/>
														 </div>
													 </c:if>
													 </div>				
												</div>

												<div class="gridImageTitle" align="center">
													<c:url value="/imwiDetailView" var="itemUrl">
														<c:param name="ct"
															value='<%="id:" + doc.getFieldValue("id")%>' />
													</c:url>
													<a href='<c:out value="${itemUrl}"/>'
														title="<%=doc.getFieldValue("title")%>">
														<%=IWMIUtils.getGridDataTitle(doc.getFieldValue("title")) %>
													</a>
												</div>
											</div>
										</td>
									</c:if>
								</c:forEach>
							</tr>
						</c:forEach>
					</tbody>
				</table>
                </div>
				<table
					style="font-size: 12px; font-weight: bold; margin: 0 auto; width: 960px;">
					<tbody>
						<tr>
							<!-- Pagination row -->
							<td colspan="4" align="right"><c:if
									test="${requestScope.hasPrevPg == true}">
									<c:url value="/iwmiDataView" var="prevPgUrl">
										<c:param name="ct" value="${requestScope.ct}" />
										<c:param name="st" value="${requestScope.st}" />
										<c:param name="srt" value="${requestScope.srt}" />
										<c:param name="srtb" value="${requestScope.srtb}" />
										<c:param name="sp" value="${requestScope.prevPg}" />
										<c:param name="sd" value="${requestScope.sd}" />
									</c:url>
									<a class="pagination-buttons"
										href='<c:out value="${prevPgUrl}"/>'>&lt;&lt;Prev</a>
								</c:if> &nbsp;Page&nbsp;<c:out value="${requestScope.currentPg}" />&nbsp;of&nbsp;
								<c:out value="${requestScope.totalPg}" /> &nbsp; <c:if
									test="${requestScope.hasNextPg == true}">
									<c:url value="/iwmiDataView" var="nextPgUrl">
										<c:param name="ct" value="${requestScope.ct}" />
										<c:param name="st" value="${requestScope.st}" />
										<c:param name="srt" value="${requestScope.srt}" />
										<c:param name="srtb" value="${requestScope.srtb}" />
										<c:param name="sp" value="${requestScope.nextPg}" />
										<c:param name="sd" value="${requestScope.sd}" />
									</c:url>
									<a class="pagination-buttons"
										href='<c:out value="${nextPgUrl}"/>'>Next&gt;&gt;</a>
								</c:if> <c:url value="/iwmiDataView" var="showAllUrl">
									<c:param name="ct" value="${requestScope.ct}" />
									<c:param name="st" value="${requestScope.st}" />
									<c:param name="srt" value="${requestScope.srt}" />
									<c:param name="srtb" value="${requestScope.srtb}" />
									<c:param name="sp" value="0" />
									<c:param name="sd" value="${requestScope.sd}" />
									<c:param name="sa" value="t" />
								</c:url> <c:if test="${requestScope.sa != 't'}">
									<a class="pagination-buttons"
										href='<c:out value="${showAllUrl}"/>'>&nbsp;&nbsp;Show
										All&nbsp;&nbsp;</a>
								</c:if></td>
							<td><div id="backHomeStyle">
									<div class="backButton">
										<a href="index.jsp">Home</a>
									</div>
								</div></td>
						</tr>
					</tbody>
				</table>
			</c:when>
			<c:otherwise>
				<div class="noRecords">No Records Found</div>
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<div id="backHomeStyle">
					<div class="backButton">
						<a href="JavaScript:void(o);" onclick="history.go(-1)">Back</a>
					</div>
				</div>
			</c:otherwise>

		</c:choose>
	</div>
</div>