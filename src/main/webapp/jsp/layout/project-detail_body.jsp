<%-- 
  hashCode Systems.
  
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 
 --%>

<%@page import="java.io.PrintStream"%>
<%@page import="com.hashcode.iwmi.wds.bean.FieldDetail"%>
<%@page import="org.apache.solr.common.SolrDocument"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.hashcode.iwmi.wds.utils.IWMIUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@page import="org.apache.solr.common.SolrDocumentList"%>
<%@page import="com.hashcode.iwmi.wds.utils.IWMIConst"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<div>
	<%
		Map<String, FieldDetail> dataMap = (Map<String, FieldDetail>) request
				.getAttribute(IWMIConst.DOC_DATA.getDesc());
		List<String> fieldOrder = (List<String>) request
				.getAttribute(IWMIConst.FIELD_ORDER.getDesc());
		Iterator<String> fieldOrderItr = fieldOrder.iterator();
	%>
	<div class="detailView">
		<div id="container">
			<div class="detailTopic">
				<%
            if (fieldOrderItr.hasNext()) {
                FieldDetail fd = dataMap.get(fieldOrderItr.next());
                if (null != fd && "title".equals(fd.getFieldName())) {
                    out.println("<div>" + fd.getFieldValue() + "</div><br/>");
                }
            }
        %>
			</div>
			<hr class="hr-style" />
			<%
			FieldDetail fd2 = dataMap.get("preview_url");
			if (null != fd2) {
		%>

			<div class="imageWrapper">
				<a><img src='<%=fd2.getFieldValue()%>' 
				    alt="" onerror="this.onerror=null;this.src='images/default-prev-image.jpg'"
					width="230px" align="right" /></a>
			</div>
		</div>
		<%
			}
		%>
		<div class="detailDescription">
			<%
				while (fieldOrderItr.hasNext()) {
					FieldDetail fd = dataMap.get(fieldOrderItr.next());
					if (null == fd)
						continue;
					else if (fd.isCaptionRequired()) {
						out.println("<div><b>" + fd.getDisplayName()
								+ ": </b>" + fd.getFieldValue() + "</div><br/>");
					} else {
						out.println("<div><b>" + fd.getDisplayName()
								+ ": </b></div>");
						out.println("<div>" + fd.getFieldValue()
								+ "</div><br/>");
					}
				}
			%>
		</div>

	</div>
</div>
<div id="backHomeStyle">
	<div class="backButton">
		<a href="index.jsp">Home</a>
	</div>
	<div class="backButton">
		<a href="JavaScript:void(o);" onclick="history.go(-1)">Back</a>
	</div>
</div>