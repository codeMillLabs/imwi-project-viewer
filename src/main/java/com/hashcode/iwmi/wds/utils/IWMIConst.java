/**
 * 
 */
package com.hashcode.iwmi.wds.utils;

/**
 * <p>
 * System wide constants 
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 *
 */
public enum IWMIConst {
	
	TOTAL_NO_REC("total_no_recs"),
	START_POINT("start_point"),
	STARTING_POINT("sp"),
	FILTER("fl"),
	DATA_SET("data_set"),
	DATA_SET_KEY_ORDER("data_set_key_order"),
	DOC_DATA("doc_data"),
	FIELD_ORDER("field_order"),
	DATA_SET_SIZE("data_set_size"),
	DATA_GRID_ROWS_COUNT("datagrid_row_count"),
	DATA_GRID_COL_COUNT("datagrid_col_count"),
	DATA_LIST_ROW_COUNT("datalst_row_count"),
	SEARCH_TEXT("st"),
	SORT_BY("srtb"),
	CONTENT_TYPE("ct"),
	SORT_KEY("srt"),
	SEARCH_TYPE("sd"),
	CURRENT_PG("currentPg"),
	PRE_PG("prevPg"),
	HAS_PREV_PG("hasPrevPg"),
	NEXT_PG("nextPg"),
	HAS_NEXT_PG("hasNextPg"),
	TOTAL_PGS("totalPg"),
	NO_DATA("no_data"),
	SHOW_ALL("sa"),
	DATA_GRID("grid"),
	DATA_LIST("lst"),
	DATA_VIEW("v"),
	URL_FIELD("url"),
	CATEGORY_SEARCH("cat"),
	CATEGORY_LST("categ_lst"),
	NO_CATEGORY_LST("no_categs"),
	TITLE_NOTE("title_note"),
	RESOURCE_TYPE("resource_type"),
	NO_GROUPING("No Group"),
	SHORTCUT_ICONS("shortcut_icons");
	
	private String desc;
	
	private IWMIConst(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}
	
}
