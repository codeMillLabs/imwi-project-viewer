/**
 * 
 */
package com.hashcode.iwmi.wds.utils;

import java.util.Comparator;

/**
 * <p>
 * Descending order comparator.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 * 
 */
public class DescendingComparator implements Comparator<String> {

	@Override
	public int compare(String str1, String str2) {

		int orderVal = str1.compareToIgnoreCase(str2);

		if (orderVal <= -1)
			return 1;
		else if (orderVal >= 1)
			return -1;
		else 
			return 0;
	}

}
