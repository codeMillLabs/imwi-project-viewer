/**
 * 
 */
package com.hashcode.iwmi.wds.utils;

import static com.hashcode.iwmi.wds.bean.SortType.ASC;

import java.util.Comparator;
import java.util.Date;

import org.apache.solr.common.SolrDocument;

import com.hashcode.iwmi.wds.bean.SortType;

/**
 * @author Amila
 * 
 */
public class SolrDocumentComparator implements Comparator<SolrDocument> {

	private String field;
	private SortType sortType;

	public SolrDocumentComparator(final String field, final SortType sortType) {
		this.field = field;
		this.sortType = sortType;
	}

	@Override
	public int compare(SolrDocument sd1, SolrDocument sd2) {

		Object obj1 = sd1.getFieldValue(this.field);
		Object obj2 = sd2.getFieldValue(this.field);

		if (null != obj1 && null != obj2) {
			int orderVal = -1;
			if (obj1 instanceof Date) {
				Date dt1 = (Date) obj1;
				Date dt2 = (Date) obj2;

				orderVal = compareDates(dt1, dt2);
			} else if (obj1 instanceof String) {
				String str1 = (String) obj1;
				String str2 = (String) obj2;

				orderVal = str1.compareToIgnoreCase(str2);
			}
			return sorting(orderVal);
		}
		return -1;
	}

	private int compareDates(Date dt1, Date dt2) {
		if (dt1.getTime() < dt2.getTime())
			return 1;
		else if (dt1.getTime() > dt2.getTime())
			return -1;
		else
			return 0;
	}

	private int sorting(int orderVal) {
		if (ASC == sortType)
			return orderVal;
		else {

			if (orderVal <= -1)
				return 1;
			else if (orderVal >= 1)
				return -1;
			else
				return 0;

		}
	}

}
