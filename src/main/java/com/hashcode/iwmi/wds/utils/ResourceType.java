/**
 * 
 */
package com.hashcode.iwmi.wds.utils;

/**
 * <p>
 * Enum class to maintain the IWMI Resource Types
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 * 
 */
public enum ResourceType {

	WATER_DATA("Water Data"), E_PROJECT("Research Project"), PHOTOS("Photos"), VIDEO(
			"Video"), AUDIO("Audio"), POWER_POINTS("Power Points"), MAPS("Maps"), POSTERS(
			"Posters"), SUCCESS_STORIES("Success Stories"), MEDIA("Media"), ALL(
			"All");

	private String desc;

	private ResourceType(final String desc) {
		this.desc = desc;
	}

	public static ResourceType getType(String desc) {
		for (ResourceType rt : ResourceType.values())
			if (rt.getDesc().equalsIgnoreCase(desc))
				return rt;
		return ALL;
	}

	/**
	 * <p>
	 * Getter for the Description.
	 * </p>
	 * 
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return this.desc;
	}

}
