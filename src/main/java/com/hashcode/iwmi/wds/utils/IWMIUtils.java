/**
 * 
 */
package com.hashcode.iwmi.wds.utils;

import static com.hashcode.iwmi.wds.utils.IWMIConst.HAS_NEXT_PG;
import static com.hashcode.iwmi.wds.utils.IWMIConst.HAS_PREV_PG;
import static com.hashcode.iwmi.wds.utils.IWMIConst.NEXT_PG;
import static com.hashcode.iwmi.wds.utils.IWMIConst.PRE_PG;
import static com.hashcode.iwmi.wds.utils.ResourceType.ALL;
import static com.hashcode.iwmi.wds.utils.ResourceType.AUDIO;
import static com.hashcode.iwmi.wds.utils.ResourceType.E_PROJECT;
import static com.hashcode.iwmi.wds.utils.ResourceType.PHOTOS;
import static com.hashcode.iwmi.wds.utils.ResourceType.MAPS;
import static com.hashcode.iwmi.wds.utils.ResourceType.MEDIA;
import static com.hashcode.iwmi.wds.utils.ResourceType.POSTERS;
import static com.hashcode.iwmi.wds.utils.ResourceType.POWER_POINTS;
import static com.hashcode.iwmi.wds.utils.ResourceType.SUCCESS_STORIES;
import static com.hashcode.iwmi.wds.utils.ResourceType.VIDEO;
import static com.hashcode.iwmi.wds.utils.ResourceType.WATER_DATA;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.solr.common.SolrDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.iwmi.wds.utils.ResourceType;

/**
 * <p>
 * Utility class of the system
 * </p>
 * 
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 */
public final class IWMIUtils {

	private static final Logger logger = LoggerFactory
			.getLogger(IWMIUtils.class);
	private static final String propFile = "./iwmi.properties";
	private static Properties properties;
	private static Long milisIn1980Yr = get1980Year();

	static {
		try {
			properties = new Properties();
			properties.load(IWMIUtils.class.getClassLoader()
					.getResourceAsStream(propFile));
			logger.info("IWMI properties loaded successfully");
		} catch (IOException e) {
			logger.error("Error encountered in loading the properties,", e);
		}
	}

	/**
	 * <p>
	 * Get Value for given property
	 * </p>
	 * 
	 * @param key
	 *            property
	 * @return value
	 */
	public static String getPropertyValue(final String key) {

		if (null != key)
			return properties.getProperty(key);
		else
			return null;
	}

	/**
	 * <p>
	 * Get Max Row count for image gallery
	 * </p>
	 * 
	 * @return row count
	 */
	public static int getMaxImageGalleryItemCount() {
		return Integer.parseInt(properties
				.getProperty("imwi.max.result.row.images.gallery.count"));
	}

	/**
	 * <p>
	 * Get Max Row count for show all system wide
	 * </p>
	 * 
	 * @return row count
	 */
	public static int getMaxRowShowAllCount() {
		return Integer.parseInt(properties
				.getProperty("imwi.max.result.show.all.row.count"));
	}

	/**
	 * <p>
	 * Get column count for data grid.
	 * </p>
	 * 
	 * @return column count
	 */
	public static int getColumnCount() {
		return Integer.parseInt(properties
				.getProperty("imwi.gallery.column.count"));
	}

	/**
	 * <p>
	 * Get row count for list view.
	 * </p>
	 * 
	 * @return row count
	 */
	public static int getListViewRowCount() {
		return Integer.parseInt(properties
				.getProperty("imwi.max.result.list.view.row.count"));
	}

	/**
	 * <p>
	 * Set next page starting pointer and the flag
	 * </p>
	 * 
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param totalNoRec
	 *            total no of records
	 * @param startingPoint
	 *            starting point
	 * @param rowCount
	 *            max row count to display
	 */
	public static void setNextPage(final HttpServletRequest request,
			final long totalNoRec, final long startingPoint, final int rowCount) {
		long nextStartingPoint = startingPoint + rowCount;
		if (totalNoRec > nextStartingPoint) {
			request.setAttribute(HAS_NEXT_PG.getDesc(), Boolean.TRUE);
			request.setAttribute(NEXT_PG.getDesc(), nextStartingPoint);
		} else {
			request.setAttribute(HAS_NEXT_PG.getDesc(), Boolean.FALSE);
		}
	}

	/**
	 * <p>
	 * Set prev page starting pointer and the flag
	 * </p>
	 * 
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param totalNoRec
	 *            total no of records
	 * @param startingPoint
	 *            starting point
	 * @param rowCount
	 *            max row count to display
	 */
	public static void setPrevPage(final HttpServletRequest request,
			final long totalNoRec, final long startingPoint, final int rowCount) {
		long prevStartingPoint = startingPoint - rowCount;
		if (prevStartingPoint >= 0) {
			request.setAttribute(HAS_PREV_PG.getDesc(), Boolean.TRUE);
			request.setAttribute(PRE_PG.getDesc(), prevStartingPoint);
		} else {
			request.setAttribute(HAS_PREV_PG.getDesc(), Boolean.FALSE);
		}
	}

	/**
	 * <p>
	 * Get the total no of pages available for search
	 * </p>
	 * 
	 * @param totalNoRec
	 *            total no of records
	 * @param rowCount
	 *            max row count to display
	 * @return no of pages
	 */
	public static int getTotalNoPages(final long totalNoRec, final int rowCount) {
		return (int) ((totalNoRec % rowCount == 0) ? totalNoRec / rowCount
				: (totalNoRec / rowCount) + 1);
	}

	/**
	 * <p>
	 * Get the data grid rows per page.
	 * </p>
	 * 
	 * @param dataSize
	 * @return data grid rows
	 */
	public static int getDataGridRows(final int dataSize) {
		return (dataSize % getColumnCount() == 0) ? dataSize / getColumnCount()
				: (dataSize / getColumnCount()) + 1;
	}

	/**
	 * <p>
	 * Get the current page no.
	 * </p>
	 * 
	 * @param startingPoint
	 *            starting point
	 * @param rowCount
	 *            max row count to display
	 * @return current page no
	 * 
	 */
	public static int getCurrentPage(final long startingPoint,
			final int rowCount) {
		return (int) (startingPoint / rowCount + 1);
	}

	/**
	 * <p>
	 * Check for String value is null or empty.
	 * </p>
	 * 
	 * @param str
	 *            value to check
	 * @return true if value is null or empty otherwise false
	 */
	public static boolean isNullOrEmpty(final String str) {
		return (null == str || str.isEmpty()) ? true : false;
	}

	/**
	 * <p>
	 * Format the date according the system wide format.
	 * </p>
	 * 
	 * @param date
	 *            date to be formatted
	 * @return formatted date in String format
	 */
	public static String getFormattedDate(final Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(
				getPropertyValue("iwmi.system.date.format"));
		return sdf.format(date);
	}

	/**
	 * <p>
	 * Get the Field order of the Project details.
	 * </p>
	 * 
	 * @return List of fields
	 */
	public static List<String> getFieldOrder() {
		String fieldsStr = getPropertyValue("iwmi.fields.order");
		List<String> fieldList = new ArrayList<String>();

		StringTokenizer tokenizer = new StringTokenizer(fieldsStr, ",");
		while (tokenizer.hasMoreTokens())
			fieldList.add(tokenizer.nextToken().trim());
		return fieldList;
	}

	/**
	 * <p>
	 * Get Title for the Grid Title.
	 * </p>
	 * 
	 * @param rawTitle
	 *            raw title from Solr
	 * @return formatted title
	 */
	public static String getGridDataTitle(final Object rawTitle) {
		int titleLen = Integer
				.parseInt(getPropertyValue("iwmi.grid.title.lenght"));
		String title = (null != rawTitle) ? (String) rawTitle : null;
		return getFormattedTitle(title, titleLen);
	}

	/**
	 * <p>
	 * Get Formatted title for display in UI.
	 * </p>
	 * 
	 * @param doc
	 *            {@link SolrDocument}
	 * @param resourceType
	 *            {@link ResourceType}
	 * @return formatted title
	 */
	public static String getListViewTitle(final SolrDocument doc,
			final ResourceType resourceType) {
		int titleLen = Integer
				.parseInt(getPropertyValue("iwmi.lst.title.lenght"));

		Object mainField = doc
				.getFieldValue(getPropertyValue("iwmi.list.main.field"));
		Object appendField = doc
				.getFieldValue(getPropertyValue("iwmi.list.main.field.append.media"));

		String mainFldStr = "";
		mainFldStr = (null == mainField) ? "" : (String) mainField;
		mainFldStr = getFormattedTitle(mainFldStr, titleLen);

		switch (resourceType) {
		case MEDIA:
			if (null != appendField && appendField instanceof Date) {
				Date dateFromDoc = (Date) appendField;
				mainFldStr = (null != dateFromDoc && dateFromDoc.getTime() > milisIn1980Yr) ? mainFldStr
						+ " - " + getFormattedDate(dateFromDoc)
						: mainFldStr;
			}
			break;

		default:
			return mainFldStr;
		}
		return mainFldStr;
	}

	private static Long get1980Year() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 1980);
		return calendar.getTimeInMillis();
	}

	private static String getFormattedTitle(final String rawTitle, int titleLen) {
		if (isNullOrEmpty(rawTitle))
			return "...";
		if (rawTitle.trim().length() > titleLen)
			return filterNonASCII(rawTitle.substring(0, titleLen));
		else
			return filterNonASCII(rawTitle);
	}

	/**
	 * <p>
	 * Filter out the non ASCII characters from the string.
	 * </p>
	 * 
	 * @param value
	 *            string with non ASCII
	 * @return string with out ASCII
	 */
	public static String filterNonASCII(String value) {
		return value.replaceAll("[^\\x00-\\x7f]", "");
	}
	
	/**
	 * Get the respective resource type according to String type.
	 * 
	 * @param type string type
	 * @return {@link ResourceType}
	 */
	public static ResourceType getResourceTypeFromData(String type) {

		if ("datasets".equalsIgnoreCase(type))
			return WATER_DATA;
		else if ("project".equalsIgnoreCase(type))
			return E_PROJECT;
		else if ("1".equalsIgnoreCase(type))
			return PHOTOS;
		else if ("3".equalsIgnoreCase(type))
			return VIDEO;
		else if ("4".equalsIgnoreCase(type))
			return AUDIO;
		else if ("5".equalsIgnoreCase(type))
			return POWER_POINTS;
		else if ("6".equalsIgnoreCase(type))
			return MAPS;
		else if ("7".equalsIgnoreCase(type))
			return POSTERS;
		else if ("8".equalsIgnoreCase(type))
			return SUCCESS_STORIES;
		else if ("11".equalsIgnoreCase(type))
			return MEDIA;
		else
			return ALL;
	}


	public static void main(String[] args) {
		String serverUrl = IWMIUtils.getPropertyValue("imwi.solr.server.url");
		logger.debug("Solr Server :" + serverUrl);

	}
}
