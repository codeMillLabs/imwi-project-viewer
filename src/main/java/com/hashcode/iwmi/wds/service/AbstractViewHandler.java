/**
 * 
 */
package com.hashcode.iwmi.wds.service;

import static com.hashcode.iwmi.wds.bean.SortType.ASC;
import static com.hashcode.iwmi.wds.bean.SortType.DESC;
import static com.hashcode.iwmi.wds.utils.IWMIConst.CATEGORY_SEARCH;
import static com.hashcode.iwmi.wds.utils.IWMIConst.CONTENT_TYPE;
import static com.hashcode.iwmi.wds.utils.IWMIConst.CURRENT_PG;
import static com.hashcode.iwmi.wds.utils.IWMIConst.DATA_SET_SIZE;
import static com.hashcode.iwmi.wds.utils.IWMIConst.DATA_VIEW;
import static com.hashcode.iwmi.wds.utils.IWMIConst.RESOURCE_TYPE;
import static com.hashcode.iwmi.wds.utils.IWMIConst.SEARCH_TEXT;
import static com.hashcode.iwmi.wds.utils.IWMIConst.SEARCH_TYPE;
import static com.hashcode.iwmi.wds.utils.IWMIConst.SHOW_ALL;
import static com.hashcode.iwmi.wds.utils.IWMIConst.SORT_BY;
import static com.hashcode.iwmi.wds.utils.IWMIConst.SORT_KEY;
import static com.hashcode.iwmi.wds.utils.IWMIConst.START_POINT;
import static com.hashcode.iwmi.wds.utils.IWMIConst.TITLE_NOTE;
import static com.hashcode.iwmi.wds.utils.IWMIConst.TOTAL_NO_REC;
import static com.hashcode.iwmi.wds.utils.IWMIConst.TOTAL_PGS;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.getCurrentPage;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.getPropertyValue;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.getTotalNoPages;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.isNullOrEmpty;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.setNextPage;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.setPrevPage;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import com.hashcode.iwmi.wds.bean.SortKey;
import com.hashcode.iwmi.wds.utils.IWMIUtils;
import com.hashcode.iwmi.wds.utils.ResourceType;

/**
 * <p>
 * Abstract View handler class handle view based on the request.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 * 
 */
public abstract class AbstractViewHandler {

	protected static final String OTHER_GROUP_FLD = getPropertyValue("iwmi.other.group.field");
	protected static final String E_PRJ_GROUP_FLD = getPropertyValue("iwmi.eproject.group.field");
	protected static final String E_PRJ_SORT_FLD = getPropertyValue("iwmi.eproject.sort.field");
	protected static final String PHOTO_SORT_FLD = getPropertyValue("iwmi.photos.sort.field");
	protected static final String WATER_DATA_GROUP_FLD = getPropertyValue("iwmi.water.data.group.field");
	protected static final String PPT_GROUP_FLD = getPropertyValue("iwmi.ppt.group.field");

	protected ResourceType resourceType;

	/**
	 * <p>
	 * Constructor
	 * </p>
	 * 
	 * @param resourceType
	 *            resource type
	 */
	public AbstractViewHandler(final ResourceType resourceType) {
		this.resourceType = resourceType;
	}

	/**
	 * Handle all the requests by the respective handler implementations
	 * 
	 * @param request
	 *            http request
	 * @param response
	 *            http response
	 * @throws Exception
	 *             exception in any error
	 */
	public abstract void handleRequest(final HttpServletRequest request,
			final HttpServletResponse response) throws Exception;

	/**
	 * <p>
	 * Setter Required attributes for every request.
	 * </p>
	 * 
	 * @param request
	 *            http request
	 * @param searchText
	 *            search text
	 * @param sortBy
	 *            sort key
	 * @param contentType
	 *            content type
	 * @param searchType
	 *            search keyword
	 * @param sortKey
	 *            sort key user input
	 * @param dataViewType
	 *            data View Type
	 */
	protected void setMandatoryAttributes(final HttpServletRequest request,
			final String searchText, final String sortBy,
			final String contentType, final String searchType,
			final String sortKey, final String dataViewType,
			final String categType) {
		request.setAttribute(DATA_VIEW.getDesc(), dataViewType);
		request.setAttribute(CONTENT_TYPE.getDesc(), contentType);
		request.setAttribute(SEARCH_TEXT.getDesc(), searchText);
		request.setAttribute(SORT_KEY.getDesc(), sortKey);
		request.setAttribute(SORT_BY.getDesc(), sortBy);
		request.setAttribute(SEARCH_TYPE.getDesc(), searchType);
		request.setAttribute(RESOURCE_TYPE.getDesc(), resourceType);
		request.setAttribute(TITLE_NOTE.getDesc(), getTitleNote());
		request.setAttribute(CATEGORY_SEARCH.getDesc(), categType);
	}

	protected void setDataInRequestAttribute(final HttpServletRequest request,
			final SolrDocumentList docList, long totalNoRecs, long startingPoint) {
		request.setAttribute(TOTAL_NO_REC.getDesc(), totalNoRecs);
		request.setAttribute(START_POINT.getDesc(), startingPoint);
		setDataSet(request, docList);
		request.setAttribute(DATA_SET_SIZE.getDesc(), docList.size());
		setRowNColumnCount(request, docList.size());
	}

	/**
	 * <p>
	 * Implementation should fill with the data population.
	 * </p>
	 * 
	 * @param request
	 *            http request
	 * @param docList
	 *            {@link SolrDocument} data set
	 */
	protected abstract void setDataSet(final HttpServletRequest request,
			final SolrDocumentList docList);

	/**
	 * <p>
	 * Set the Row and Column count of the data view components.
	 * </p>
	 * 
	 * @param request
	 *            http request
	 * @param dataCount
	 *            data size
	 * 
	 */
	protected abstract void setRowNColumnCount(
			final HttpServletRequest request, final int dataCount);

	/**
	 * <p>
	 * Setter Pagination related attributes.
	 * </p>
	 * 
	 * @param request
	 *            http request
	 * @param showAll
	 *            show all feature
	 * @param maxRowCount
	 *            max row count per page
	 * @param totalNoRecs
	 *            total no of records found
	 * @param startingPoint
	 *            starting point of the page
	 */
	protected void setPaginationAttributes(final HttpServletRequest request,
			final String showAll, final int maxRowCount,
			final long totalNoRecs, final long startingPoint) {
		request.setAttribute(TOTAL_PGS.getDesc(),
				getTotalNoPages(totalNoRecs, maxRowCount));
		request.setAttribute(CURRENT_PG.getDesc(),
				getCurrentPage(startingPoint, maxRowCount));
		request.setAttribute(SHOW_ALL.getDesc(), showAll);
		if (IWMIUtils.isNullOrEmpty(showAll)) {
			setNextPage(request, totalNoRecs, startingPoint, maxRowCount);
			setPrevPage(request, totalNoRecs, startingPoint, maxRowCount);
		}
	}

	/**
	 * <p>
	 * Method to retrive the sorting fields in a query compatible way.
	 * </p>
	 * 
	 * @param sortKeys
	 *            list of sort keys
	 * @return sort query string
	 */
	protected String getSortingKeyString(final String sortKeyStr,
			final String categSearchStr, final String sortText) {
		StringBuffer buffer = new StringBuffer();
		boolean isMultiCateg = isNullOrEmpty(categSearchStr);
		boolean isSortText = isNullOrEmpty(sortText);
		Iterator<SortKey> itr = getSortKeys(sortKeyStr, isMultiCateg,
				isSortText).iterator();
		while (itr.hasNext()) {
			SortKey sortKey = itr.next();
			buffer.append(sortKey.getField()).append("%20")
					.append(sortKey.getType().getDesc());
			if (itr.hasNext())
				buffer.append(",");
		}
		return buffer.toString();
	}

	private List<SortKey> getSortKeys(final String sortKeyStr,
			final boolean isMultiCategory, final boolean isSortTxt) {
		List<SortKey> sortKeys = new ArrayList<SortKey>();

		if (isMultiCategory || !isSortTxt) {
			switch (resourceType) {
			case WATER_DATA:
				sortKeys.add(new SortKey(WATER_DATA_GROUP_FLD, ASC));
				break;
			case E_PROJECT:
				sortKeys.add(new SortKey(E_PRJ_GROUP_FLD, ASC));
				break;
			case AUDIO:
			case MEDIA:
			case POSTERS:
			case SUCCESS_STORIES:
			case VIDEO:
				sortKeys.add(new SortKey(OTHER_GROUP_FLD, DESC));
				break;
			case POWER_POINTS:
				sortKeys.add(new SortKey(PPT_GROUP_FLD, DESC));
				break;
			case PHOTOS:
				if (isNullOrEmpty(sortKeyStr))
					sortKeys.add(new SortKey(PHOTO_SORT_FLD, DESC));
				break;
			case MAPS:
			case ALL:
			default:
				break;
			}
			if (!isNullOrEmpty(sortKeyStr))
				sortKeys.add(new SortKey(sortKeyStr, ASC));
		}
		return sortKeys;
	}

	private String getTitleNote() {
		String titleNote = "";
		switch (resourceType) {
		case WATER_DATA:
			titleNote = IWMIUtils.getPropertyValue("iwmi.tile.note.water.data");
			break;
		case E_PROJECT:
			titleNote = IWMIUtils.getPropertyValue("iwmi.tile.note.eproject");
			break;
		case PHOTOS:
			titleNote = IWMIUtils.getPropertyValue("iwmi.tile.note.photos");
			break;
		case VIDEO:
			titleNote = IWMIUtils.getPropertyValue("iwmi.tile.note.video");
			break;
		case AUDIO:
			titleNote = IWMIUtils.getPropertyValue("iwmi.tile.note.audio");
			break;
		case POWER_POINTS:
			titleNote = IWMIUtils.getPropertyValue("iwmi.tile.note.ppt");
			break;
		case MAPS:
			titleNote = IWMIUtils.getPropertyValue("iwmi.tile.note.map");
			break;
		case POSTERS:
			titleNote = IWMIUtils.getPropertyValue("iwmi.tile.note.posters");
			break;
		case SUCCESS_STORIES:
			titleNote = IWMIUtils.getPropertyValue("iwmi.tile.note.success");
			break;
		case MEDIA:
			titleNote = IWMIUtils.getPropertyValue("iwmi.tile.note.media");
			break;
		case ALL:
			titleNote = IWMIUtils.getPropertyValue("iwmi.tile.note.all");
			break;
		default:
			return "";
		}
		return (isNullOrEmpty(titleNote)) ? "" : titleNote;
	}

}
