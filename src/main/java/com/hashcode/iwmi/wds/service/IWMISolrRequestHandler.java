/**
 * 
 */
package com.hashcode.iwmi.wds.service;

import static com.hashcode.iwmi.wds.utils.IWMIUtils.getPropertyValue;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.isNullOrEmpty;

import java.io.Serializable;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.servlet.SolrRequestParsers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Class to handle all the Solr requests and responses.
 * </p>
 * 
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 * 
 */
public class IWMISolrRequestHandler implements Serializable {

	private static final long serialVersionUID = 3830107091127346826L;
	private static final Logger logger = LoggerFactory
			.getLogger(IWMISolrRequestHandler.class);

	private static HttpSolrServer server;
	private String serverUrl;

	/**
	 * <p>
	 * Constructor
	 * </p>
	 */
	private IWMISolrRequestHandler() {
		if (null == server) {
			this.serverUrl = getPropertyValue("imwi.solr.server.url");
			server = new HttpSolrServer(serverUrl);
			server.setParser(new XMLResponseParser());
		}
	}

	/**
	 * <p>
	 * Create new IWMI Solr Request Handler.
	 * </p>
	 * 
	 * @return {@link IWMISolrRequestHandler}
	 */
	public static IWMISolrRequestHandler createHandler() {
		return new IWMISolrRequestHandler();
	}

	/**
	 * <p>
	 * Create Solr compatible query.
	 * </p>
	 * 
	 * @param contentType
	 *            requesting content type or wild card search
	 * @param facedQuery
	 *            faced query for search specific data
	 * @param sortKey
	 *            sort give field by ascending order
	 * @param startPos
	 *            starting position of document
	 * @param maxResultCount
	 *            max results per query
	 * @param filter
	 *            filter specific fields commas separated list
	 * @return Solr supported query
	 */
	public String createQuery(final String contentType,
			final String facedQuery, final String sortKey, final int startPos,
			final int maxResultCount, final String filter) {
		StringBuilder query = new StringBuilder();
		String qry = (!isNullOrEmpty(contentType)) ? contentType.trim()
				: "*:*";
		query.append("q=").append(qry);

		if (!isNullOrEmpty(facedQuery))
			query.append("&fq=").append(facedQuery.trim());

		if (!isNullOrEmpty(sortKey))
			query.append("&sort=").append(sortKey.trim());

		if (!isNullOrEmpty(filter))
			query.append("&fl=").append(filter.trim());

		query.append("&start=").append(startPos);
		query.append("&rows=").append(maxResultCount);

		logger.debug("Solr Request query : {}", query.toString());
		return query.toString();
	}

	/**
	 * <p>
	 * Query from the Solr server.
	 * </p>
	 * 
	 * @param query
	 *            query parameters that need to send to server
	 * @return {@link QueryResponse}
	 * @throws SolrServerException
	 *             throws in any error
	 */
	public QueryResponse querySolr(final String query)
			throws SolrServerException {
		logger.debug("Query to Solr [{}]", query);
		SolrParams solrParams = SolrRequestParsers.parseQueryString(query);
		return server.query(solrParams);
	}

	/**
	 * <p>
	 * Get Documents from the response.
	 * </p>
	 * 
	 * @param response
	 *            query response
	 * @return {@link SolrDocumentList}
	 */
	public SolrDocumentList getDocuments(final QueryResponse response) {
		if (null == response) {
			return null;
		}
		return response.getResults();
	}

	/**
	 * <p>
	 * Get the no of total records found for query.
	 * </p>
	 * 
	 * @param solrDocs
	 *            {@link SolrDocumentList}
	 * @return no of total records found
	 */
	public long noOfRecordsFound(final SolrDocumentList solrDocs) {
		if (null == solrDocs)
			return 0;
		else
			return solrDocs.getNumFound();
	}

	/**
	 * <p>
	 * Get the starting point of the data set.
	 * </p>
	 * 
	 * @param solrDocs
	 *            {@link SolrDocumentList}
	 * @return starting point
	 */
	public long getStartingPosition(final SolrDocumentList solrDocs) {
		if (null == solrDocs)
			return 0;
		else
			return solrDocs.getStart();
	}

	public static void main(String[] args) {
		IWMISolrRequestHandler handler = new IWMISolrRequestHandler();
		handler.createQuery("vid", "", null, 5, 10, "id,price,kk");
		handler.createQuery("vid*", "content_type:1", "price", 0, 5, "");
		handler.createQuery("vid", null, "", 0, 20, null);
		handler.createQuery("video", "", "price", 0, 20, "de");
	}

}
