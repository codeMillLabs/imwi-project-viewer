/**
 * 
 */
package com.hashcode.iwmi.wds.service;

import static com.hashcode.iwmi.wds.service.IWMISolrRequestHandler.createHandler;
import static com.hashcode.iwmi.wds.utils.IWMIConst.CONTENT_TYPE;
import static com.hashcode.iwmi.wds.utils.IWMIConst.DATA_GRID;
import static com.hashcode.iwmi.wds.utils.IWMIConst.DATA_GRID_COL_COUNT;
import static com.hashcode.iwmi.wds.utils.IWMIConst.DATA_GRID_ROWS_COUNT;
import static com.hashcode.iwmi.wds.utils.IWMIConst.DATA_SET;
import static com.hashcode.iwmi.wds.utils.IWMIConst.FILTER;
import static com.hashcode.iwmi.wds.utils.IWMIConst.SEARCH_TEXT;
import static com.hashcode.iwmi.wds.utils.IWMIConst.SEARCH_TYPE;
import static com.hashcode.iwmi.wds.utils.IWMIConst.SHOW_ALL;
import static com.hashcode.iwmi.wds.utils.IWMIConst.SORT_BY;
import static com.hashcode.iwmi.wds.utils.IWMIConst.SORT_KEY;
import static com.hashcode.iwmi.wds.utils.IWMIConst.STARTING_POINT;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.getColumnCount;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.getDataGridRows;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.getMaxImageGalleryItemCount;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.getMaxRowShowAllCount;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.isNullOrEmpty;
import static com.hashcode.iwmi.wds.utils.IWMIConst.SHORTCUT_ICONS;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.getPropertyValue;
import static com.hashcode.iwmi.wds.utils.ResourceType.ALL;
import static com.hashcode.iwmi.wds.utils.ResourceType.AUDIO;
import static com.hashcode.iwmi.wds.utils.ResourceType.E_PROJECT;
import static com.hashcode.iwmi.wds.utils.ResourceType.PHOTOS;
import static com.hashcode.iwmi.wds.utils.ResourceType.MAPS;
import static com.hashcode.iwmi.wds.utils.ResourceType.MEDIA;
import static com.hashcode.iwmi.wds.utils.ResourceType.POSTERS;
import static com.hashcode.iwmi.wds.utils.ResourceType.POWER_POINTS;
import static com.hashcode.iwmi.wds.utils.ResourceType.SUCCESS_STORIES;
import static com.hashcode.iwmi.wds.utils.ResourceType.VIDEO;
import static com.hashcode.iwmi.wds.utils.ResourceType.WATER_DATA;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.iwmi.wds.utils.IWMIConst;
import com.hashcode.iwmi.wds.utils.ResourceType;

/**
 * <p>
 * Class for handle all the Grid view. eg: gallery
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 * 
 */
public class DataGridViewHandler extends AbstractViewHandler implements
		Serializable {

	private static final long serialVersionUID = -5953022798994745610L;
	private static final Logger logger = LoggerFactory
			.getLogger(DataGridViewHandler.class);

	/**
	 * <p>
	 * Constructor.
	 * </p>
	 * 
	 * @param resourceType
	 */
	public DataGridViewHandler(final ResourceType resourceType) {
		super(resourceType);
	}

	@Override
	public void handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.debug("Grid View Handler going to process request");
		String searchText = request.getParameter(SEARCH_TEXT.getDesc());
		String sortBy = request.getParameter(SORT_BY.getDesc());
		String contentType = request.getParameter(CONTENT_TYPE.getDesc());
		String filter = request.getParameter(FILTER.getDesc());
		String sp = request.getParameter(STARTING_POINT.getDesc());
		String showAll = request.getParameter(SHOW_ALL.getDesc());
		String searchType = request.getParameter(SEARCH_TYPE.getDesc());

		String queryData = (isNullOrEmpty(searchText)) ? contentType
				: searchText;
		String facedQuery = (!isNullOrEmpty(searchText)) ? contentType : null;
		String sortKey = (isNullOrEmpty(sortBy)) ? request
				.getParameter(SORT_KEY.getDesc()) : sortBy;
		String querySortKey = getSortingKeyString(sortKey, "", "");

		int startPos = (isNullOrEmpty(sp)) ? 0 : Integer.parseInt(sp.trim());
		int maxRowCount = (isNullOrEmpty(showAll)) ? getMaxImageGalleryItemCount()
				: getMaxRowShowAllCount();

		IWMISolrRequestHandler handler = createHandler();
		String solrQuery = handler.createQuery(queryData, facedQuery,
				querySortKey, startPos, maxRowCount, filter);
		QueryResponse queryResponse = handler.querySolr(solrQuery);
		SolrDocumentList docList = handler.getDocuments(queryResponse);
		long totalNoRecs = handler.noOfRecordsFound(docList);
		long startingPoint = handler.getStartingPosition(docList);

		setMandatoryAttributes(request, searchText, sortBy, contentType,
				searchType, sortKey, DATA_GRID.getDesc(), null);

		if (null != docList && docList.size() > 0) {
			setDataInRequestAttribute(request, docList, totalNoRecs,
					startingPoint);
			setPaginationAttributes(request, showAll, maxRowCount, totalNoRecs,
					startingPoint);
			setShortcutIconImageUrls(request);

		} else {
			// no records found for this query.
			logger.info("No records found.");
			request.setAttribute(IWMIConst.NO_DATA.getDesc(),
					"No Records Found");
		}

		RequestDispatcher dispather = request
				.getRequestDispatcher("./jsp/mainDataGrid.jsp");
		dispather.forward(request, response);
		logger.debug("Grid View Handle dispatch the request");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hashcode.iwmi.wds.service.AbstractViewHandler#setRowNColumnCount(
	 * javax.servlet.http.HttpServletRequest, int)
	 */
	@Override
	protected void setRowNColumnCount(HttpServletRequest request, int dataCount) {
		request.setAttribute(DATA_GRID_ROWS_COUNT.getDesc(),
				getDataGridRows(dataCount));
		request.setAttribute(DATA_GRID_COL_COUNT.getDesc(), getColumnCount());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hashcode.iwmi.wds.service.AbstractViewHandler#setDataSet(javax.servlet
	 * .http.HttpServletRequest, org.apache.solr.common.SolrDocumentList)
	 */
	@Override
	protected void setDataSet(HttpServletRequest request,
			SolrDocumentList docList) {
		request.setAttribute(DATA_SET.getDesc(), docList);
	}

	private void setShortcutIconImageUrls(HttpServletRequest request) {
		Map<ResourceType, String> iconUrlMap = new HashMap<ResourceType, String>();

		iconUrlMap.put(WATER_DATA, getPropertyValue("iwmi.icon.water.data"));
		iconUrlMap.put(E_PROJECT, getPropertyValue("iwmi.icon.eproject"));
		iconUrlMap.put(PHOTOS, getPropertyValue("iwmi.icon.images"));
		iconUrlMap.put(VIDEO, getPropertyValue("iwmi.icon.video"));
		iconUrlMap.put(AUDIO, getPropertyValue("iwmi.icon.audio"));
		iconUrlMap.put(POWER_POINTS, getPropertyValue("iwmi.icon.ppt"));
		iconUrlMap.put(MAPS, getPropertyValue("iwmi.icon.map"));
		iconUrlMap.put(POSTERS, getPropertyValue("iwmi.icon.posters"));
		iconUrlMap
				.put(SUCCESS_STORIES, getPropertyValue("iwmi.icon.success"));
		iconUrlMap.put(MEDIA, getPropertyValue("iwmi.icon.media"));
		iconUrlMap.put(ALL, getPropertyValue("iwmi.icon.all"));

		request.setAttribute(SHORTCUT_ICONS.getDesc(), iconUrlMap);
	}
}
