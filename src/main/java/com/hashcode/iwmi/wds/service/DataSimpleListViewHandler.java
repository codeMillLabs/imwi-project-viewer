/**
 * 
 */
package com.hashcode.iwmi.wds.service;

import static com.hashcode.iwmi.wds.bean.SortType.ASC;
import static com.hashcode.iwmi.wds.bean.SortType.DESC;
import static com.hashcode.iwmi.wds.service.IWMISolrRequestHandler.createHandler;
import static com.hashcode.iwmi.wds.utils.IWMIConst.CATEGORY_LST;
import static com.hashcode.iwmi.wds.utils.IWMIConst.CATEGORY_SEARCH;
import static com.hashcode.iwmi.wds.utils.IWMIConst.CONTENT_TYPE;
import static com.hashcode.iwmi.wds.utils.IWMIConst.DATA_LIST;
import static com.hashcode.iwmi.wds.utils.IWMIConst.DATA_LIST_ROW_COUNT;
import static com.hashcode.iwmi.wds.utils.IWMIConst.DATA_SET;
import static com.hashcode.iwmi.wds.utils.IWMIConst.DATA_SET_KEY_ORDER;
import static com.hashcode.iwmi.wds.utils.IWMIConst.FILTER;
import static com.hashcode.iwmi.wds.utils.IWMIConst.NO_CATEGORY_LST;
import static com.hashcode.iwmi.wds.utils.IWMIConst.NO_DATA;
import static com.hashcode.iwmi.wds.utils.IWMIConst.NO_GROUPING;
import static com.hashcode.iwmi.wds.utils.IWMIConst.SEARCH_TEXT;
import static com.hashcode.iwmi.wds.utils.IWMIConst.SEARCH_TYPE;
import static com.hashcode.iwmi.wds.utils.IWMIConst.SHOW_ALL;
import static com.hashcode.iwmi.wds.utils.IWMIConst.SORT_BY;
import static com.hashcode.iwmi.wds.utils.IWMIConst.SORT_KEY;
import static com.hashcode.iwmi.wds.utils.IWMIConst.STARTING_POINT;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.getListViewRowCount;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.getMaxRowShowAllCount;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.isNullOrEmpty;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.iwmi.wds.bean.SortType;
import com.hashcode.iwmi.wds.utils.DescendingComparator;
import com.hashcode.iwmi.wds.utils.IWMIUtils;
import com.hashcode.iwmi.wds.utils.ResourceType;

/**
 * <p>
 * Class for handle all the list views.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 * 
 */
public class DataSimpleListViewHandler extends AbstractViewHandler implements
		Serializable {

	private static final long serialVersionUID = 1686419380285748658L;
	private static final Logger logger = LoggerFactory
			.getLogger(DataSimpleListViewHandler.class);
	private static final String YEAR_FMT = "yyyy";
	private static final String TIME_FMT = "yyyy-MM-dd'T'HH:mm:ss.sss";
	private static final SimpleDateFormat YEAR_FORMATTER = new SimpleDateFormat(
			YEAR_FMT);
	private static final SimpleDateFormat TIME_FORMATTER = new SimpleDateFormat(
			TIME_FMT);

	/**
	 * <p>
	 * Constructor.
	 * </p>
	 * 
	 * @param resourceType
	 */
	public DataSimpleListViewHandler(final ResourceType resourceType) {
		super(resourceType);
	}

	@Override
	public void handleRequest(final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
		logger.debug("Simple List View Handler going to process request");
		String searchText = request.getParameter(SEARCH_TEXT.getDesc());
		String sortBy = request.getParameter(SORT_BY.getDesc());
		String contentType = request.getParameter(CONTENT_TYPE.getDesc());
		String filter = request.getParameter(FILTER.getDesc());
		String sp = request.getParameter(STARTING_POINT.getDesc());
		String showAll = request.getParameter(SHOW_ALL.getDesc());
		String searchType = request.getParameter(SEARCH_TYPE.getDesc());
		
		String searchTextRef = request.getParameter("st-ref");
		if(null != searchTextRef)
		{
			searchText = searchTextRef;
		}

		String categoryGrpType = request
				.getParameter(CATEGORY_SEARCH.getDesc());
		
		String searchCategText = "";
		 
		if(isNullOrEmpty(categoryGrpType)) {
			searchCategText = searchText;
		} else if(!isNullOrEmpty(searchText) && !isNullOrEmpty(categoryGrpType)) {
			searchCategText = searchText + " AND " +  getCategGrpString(categoryGrpType);
		} else {
			searchCategText =  getCategGrpString(categoryGrpType);
		}

		String queryData = getQueryData(searchText, contentType, searchCategText);
		String facedQuery = (!isNullOrEmpty(searchCategText)) ? contentType : null;
		String sortKey = (isNullOrEmpty(sortBy)) ? request
				.getParameter(SORT_KEY.getDesc()) : sortBy;
		String querySortKey = getSortingKeyString(sortKey, categoryGrpType,
				sortBy);

		int startPos = (isNullOrEmpty(sp)) ? 0 : Integer.parseInt(sp.trim());
		int maxRowCount = (isNullOrEmpty(showAll)) ? getListViewRowCount()
				: getMaxRowShowAllCount();

		IWMISolrRequestHandler handler = createHandler();
		String solrQuery = handler.createQuery(queryData, facedQuery,
				querySortKey, startPos, maxRowCount, filter);
		QueryResponse queryResponse = handler.querySolr(solrQuery);
		SolrDocumentList docList = handler.getDocuments(queryResponse);
		long totalNoRecs = handler.noOfRecordsFound(docList);
		long startingPoint = handler.getStartingPosition(docList);

		setMandatoryAttributes(request, searchText, sortBy, contentType,
				searchType, sortKey, DATA_LIST.getDesc(), categoryGrpType);

		if (null != docList && docList.size() > 0) {
			setDataInRequestAttribute(request, docList, totalNoRecs,
					startingPoint);
			setPaginationAttributes(request, showAll, maxRowCount, totalNoRecs,
					startingPoint);
			setCategoryListLoadData(request, contentType);

		} else {
			// no records found for this query.
			logger.info("No records found.");
			request.setAttribute(NO_DATA.getDesc(), "No Records Found");
		}

		RequestDispatcher dispather = request
				.getRequestDispatcher("./jsp/mainDataLst.jsp");
		dispather.forward(request, response);
		logger.debug("Simple List View Handle dispatch the request");
	}

	
	private String getQueryData(String searchText, final String contentType,
			final String searchCategText) {
		
		searchText = (isNullOrEmpty(searchCategText)) ? searchText
				: searchCategText;
		return (isNullOrEmpty(searchText)) ? contentType : searchText;
	}

	private void setCategoryListLoadData(final HttpServletRequest request,
			String contentType) throws SolrServerException {
		Set<String> categSet = getCategoryGrps(contentType);
		request.setAttribute(CATEGORY_LST.getDesc(), categSet);
		request.setAttribute(NO_CATEGORY_LST.getDesc(), categSet.size());
	}

	@Override
	protected void setDataSet(final HttpServletRequest request,
			final SolrDocumentList docList) {
		Map<String, List<SolrDocument>> dataMap = null;
		List<String> keys = null;

		switch (resourceType) {
		case WATER_DATA:
			dataMap = groupDataStringField(docList, WATER_DATA_GROUP_FLD);
			request.setAttribute(DATA_SET.getDesc(), dataMap);
			logger.debug("key set order {},", dataMap.keySet());
			request.setAttribute(DATA_SET_KEY_ORDER.getDesc(), dataMap.keySet());
			break;

		case E_PROJECT:
			dataMap = groupDataStringField(docList, E_PRJ_GROUP_FLD);
			request.setAttribute(DATA_SET.getDesc(), dataMap);

			logger.debug("key set order {},", keys);
			request.setAttribute(DATA_SET_KEY_ORDER.getDesc(), dataMap.keySet());
			break;

		case POWER_POINTS:
			dataMap = groupDataByYear(docList, PPT_GROUP_FLD);
			request.setAttribute(DATA_SET.getDesc(), dataMap);

			keys = Arrays.asList(dataMap.keySet().toArray(new String[0]));
			Collections.sort(keys, new DescendingComparator());
			logger.debug("key set order {},", keys);
			request.setAttribute(DATA_SET_KEY_ORDER.getDesc(), keys);
			break;

		case AUDIO:
		case VIDEO:
		case MEDIA:
		case POSTERS:
		case SUCCESS_STORIES:
			dataMap = groupDataByYear(docList, OTHER_GROUP_FLD);
			request.setAttribute(DATA_SET.getDesc(), dataMap);

			keys = Arrays.asList(dataMap.keySet().toArray(new String[0]));
			Collections.sort(keys, new DescendingComparator());
			logger.debug("key set order {},", keys);
			request.setAttribute(DATA_SET_KEY_ORDER.getDesc(), keys);
			break;

		default:
			dataMap = groupAllDataInALL(docList);
			request.setAttribute(DATA_SET.getDesc(), dataMap);
			request.setAttribute(DATA_SET_KEY_ORDER.getDesc(), dataMap.keySet());
			return;
		}
	}

	private String getCategGrpString(String categType) {
		switch (resourceType) {
		case WATER_DATA:
			return WATER_DATA_GROUP_FLD + ":" + categType;
		case E_PROJECT:
			return E_PRJ_GROUP_FLD + ":" + categType;
		case POWER_POINTS:
			return PPT_GROUP_FLD + ":[" + getDateRangeForYear(categType) + "]";
		case AUDIO:
		case VIDEO:
		case MEDIA:
		case POSTERS:
		case SUCCESS_STORIES:
			return OTHER_GROUP_FLD + ":[" + getDateRangeForYear(categType)
					+ "]";
		default:
			return "";
		}
	}

	private String getDateRangeForYear(final String yearVal) {
		try {
			Date selYear = YEAR_FORMATTER.parse(yearVal);
			String startDate = TIME_FORMATTER.format(getStartDate(selYear));
			String endDate = TIME_FORMATTER.format(getEndDate(selYear));
			logger.debug("Date range, Start Date :{}, End Date :{}", startDate,
					endDate);
			return startDate + "Z%20TO%20" + endDate + "Z";

		} catch (Exception e) {
			logger.error("Error in Search By Category date range creation, {}",
					e.getMessage());
			return "";
		}
	}

	private Date getStartDate(final Date dt) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		cal.add(Calendar.MILLISECOND, -1);
		return cal.getTime();
	}

	private Date getEndDate(final Date dt) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		cal.add(Calendar.YEAR, +1);
		return cal.getTime();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hashcode.iwmi.wds.service.AbstractViewHandler#setRowNColumnCount(
	 * javax.servlet.http.HttpServletRequest, int)
	 */
	@Override
	protected void setRowNColumnCount(HttpServletRequest request, int dataCount) {
		request.setAttribute(DATA_LIST_ROW_COUNT.getDesc(), dataCount);
	}

	private Set<String> getCategoryGrps(final String queryData)
			throws SolrServerException {
		String filter = "";
		SortType sortType = ASC;
		switch (resourceType) {
		case WATER_DATA:
			filter = WATER_DATA_GROUP_FLD;
			break;
		case E_PROJECT:
			filter = E_PRJ_GROUP_FLD;
			break;
		case POWER_POINTS:
			filter = PPT_GROUP_FLD;
			sortType = DESC;
			break;
		case AUDIO:
		case VIDEO:
		case MEDIA:
		case POSTERS:
		case SUCCESS_STORIES:
			filter = OTHER_GROUP_FLD;
			sortType = DESC;
			break;
		default:
			filter = "";
		}

		IWMISolrRequestHandler handler = createHandler();
		String solrQuery = handler.createQuery(queryData, null, null, 0,
				IWMIUtils.getMaxRowShowAllCount(), filter);
		QueryResponse queryResponse = handler.querySolr(solrQuery);
		SolrDocumentList docList = handler.getDocuments(queryResponse);
		return getUniqueDataList(docList, filter, sortType);
	}

	private Set<String> getUniqueDataList(final SolrDocumentList docList,
			final String field, final SortType sortType) {
		Set<String> categoryLst = (ASC == sortType) ? new TreeSet<String>()
				: new TreeSet<String>(new DescendingComparator());

		if (null != docList) {
			for (SolrDocument doc : docList) {
				Object obj = doc.getFieldValue(field);
				if (null == obj)
					continue;
				if (obj instanceof Date) {
					Calendar cal = Calendar.getInstance();
					cal.setTime((Date) obj);
					categoryLst.add("" + cal.get(Calendar.YEAR));
				} else {
					categoryLst.add(String.valueOf(obj));
				}
			}
		}
		logger.debug("Category List :" + categoryLst);
		return categoryLst;
	}

	private Map<String, List<SolrDocument>> groupAllDataInALL(
			final SolrDocumentList docList) {
		logger.debug("Going to group all data under one list");
		Map<String, List<SolrDocument>> allDocsMap = new TreeMap<String, List<SolrDocument>>();
		List<SolrDocument> docs = new ArrayList<SolrDocument>();
		for (SolrDocument doc : docList)
			docs.add(doc);
		allDocsMap.put(NO_GROUPING.getDesc(), docs);
		logger.debug("All data list :{}", allDocsMap);
		return allDocsMap;
	}

	private Map<String, List<SolrDocument>> groupDataByYear(
			final SolrDocumentList docList, final String yearFieldKey) {
		logger.debug(
				"Going to group data by year for the given field : {}, Doc list {}",
				yearFieldKey, docList.size());
		Map<String, List<SolrDocument>> docGrpByYear = new TreeMap<String, List<SolrDocument>>();
		for (SolrDocument doc : docList) {
			String yrValue = getYearFromValue(doc.getFieldValue(yearFieldKey));
			logger.debug("Grouping Year :{}, Field Value :{}", yrValue,
					doc.getFieldValue(yearFieldKey));
			List<SolrDocument> solrDocs = docGrpByYear.get(yrValue);
			if (null == solrDocs) {
				solrDocs = new ArrayList<SolrDocument>();
				docGrpByYear.put(yrValue, solrDocs);
			}
			logger.debug("Data added {} : {}.", yrValue, doc);
			solrDocs.add(doc);
		}
		logger.debug("Data Map [{}]", docGrpByYear);
		return docGrpByYear;
	}

	private Map<String, List<SolrDocument>> groupDataStringField(
			final SolrDocumentList docList, final String fieldKey) {
		logger.debug(
				"Going to group data for the given field : {}, Doc list {}",
				fieldKey, docList.size());
		Map<String, List<SolrDocument>> docGrpByField = new TreeMap<String, List<SolrDocument>>();
		for (SolrDocument doc : docList) {
			String fieldValue = (String) doc.getFieldValue(fieldKey);
			logger.debug("Grouping :{}, Field Value :{}", fieldKey, fieldValue);
			List<SolrDocument> solrDocs = docGrpByField.get(fieldValue);
			if (null == solrDocs) {
				solrDocs = new ArrayList<SolrDocument>();
				docGrpByField.put(fieldValue, solrDocs);
			}
			logger.debug("Data added {} : {}.", fieldValue, doc);
			solrDocs.add(doc);
		}
		logger.debug("Data Map [{}]", docGrpByField);
		return docGrpByField;
	}

	private String getYearFromValue(final Object dateValue) {
		if (dateValue != null && dateValue instanceof Date) {
			Date date = (Date) dateValue;
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			return "" + calendar.get(Calendar.YEAR);
		}
		return "Other";
	}
}
