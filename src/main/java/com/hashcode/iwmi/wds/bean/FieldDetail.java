package com.hashcode.iwmi.wds.bean;

import static com.hashcode.iwmi.wds.utils.IWMIConst.URL_FIELD;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.getPropertyValue;

import java.io.Serializable;

/**
 * <p>
 * Field Details bean to display in Project details.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 * 
 */
public class FieldDetail implements Serializable {

	private static final long serialVersionUID = -4134327555657983255L;

	private String displayName;
	private String fieldName;
	private String fieldValue;
	private boolean captionRequired;

	/**
	 * <p>
	 * Create a Field Details using metadata.
	 * </p>
	 * 
	 * @param fieldName
	 * @param fieldValue
	 * @return {@link FieldDetail}
	 */
	public static FieldDetail createFieldDetail(String fieldName,
			String fieldValue) {
		String metaData = getPropertyValue("iwmi.field." + fieldName);
		if (null == metaData)
			return null;
		String[] data = metaData.split(":");
		return new FieldDetail(fieldName, data[1], formatDisplayValues(
				fieldName, fieldValue), getBoolean(data[2]));
	}

	private static boolean getBoolean(final String bool) {
		return ("true".equals(bool)) ? Boolean.TRUE : Boolean.FALSE;
	}

	private static String formatDisplayValues(final String fieldName,
			final String value) {
		if (URL_FIELD.getDesc().equals(fieldName)) {
			return "<a href='" + value + "' target='blank' id='project-detail-url' >" + value + "</a>";
		}
		return value;
	}

	/**
	 * <p>
	 * Constructor with fields.
	 * </p>
	 * 
	 * @param displayName
	 * @param fieldName
	 * @param fieldValue
	 * @param captionRequired
	 */
	private FieldDetail(String fieldName, String displayName,
			String fieldValue, boolean captionRequired) {
		this.displayName = displayName;
		this.fieldName = fieldName;
		this.fieldValue = fieldValue;
		this.captionRequired = captionRequired;
	}

	/**
	 * @param fieldValue
	 *            the fieldValue to set
	 */
	public void setFieldValue(final String fieldValue) {
		this.fieldValue = fieldValue;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @return the fieldName
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * @return the fieldValue
	 */
	public String getFieldValue() {
		return fieldValue;
	}

	/**
	 * @return the captionRequired
	 */
	public boolean isCaptionRequired() {
		return captionRequired;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FieldDetail [displayName=");
		builder.append(displayName);
		builder.append(", fieldName=");
		builder.append(fieldName);
		builder.append(", fieldValue=");
		builder.append(fieldValue);
		builder.append(", captionRequired=");
		builder.append(captionRequired);
		builder.append("]");
		return builder.toString();
	}

}
