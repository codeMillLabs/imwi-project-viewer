/**
 * 
 */
package com.hashcode.iwmi.wds.bean;

/**
 * <p>
 * Class for keep the sort keys
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 */
public class SortKey {

	private String field;
	private SortType type;

	public SortKey(final String field, final SortType type) {
		this.field = field;
		this.type = type;
	}

	/**
	 * @return the field
	 */
	public String getField() {
		return field;
	}

	/**
	 * @param field
	 *            the field to set
	 */
	public void setField(String field) {
		this.field = field;
	}

	/**
	 * @return the type
	 */
	public SortType getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(SortType type) {
		this.type = type;
	}
}
