package com.hashcode.iwmi.wds.servlets;

import static com.hashcode.iwmi.wds.service.IWMISolrRequestHandler.createHandler;
import static com.hashcode.iwmi.wds.utils.IWMIConst.DOC_DATA;
import static com.hashcode.iwmi.wds.utils.IWMIConst.FIELD_ORDER;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.getFieldOrder;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.getFormattedDate;
import static com.hashcode.iwmi.wds.utils.IWMIUtils.isNullOrEmpty;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.iwmi.wds.bean.FieldDetail;
import com.hashcode.iwmi.wds.service.IWMISolrRequestHandler;
import com.hashcode.iwmi.wds.utils.IWMIConst;

/**
 * <p>
 * Servlet to display project detail view.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 * 
 */
public class IWMIDetailViewServlet extends HttpServlet {

	private static final long serialVersionUID = 2686887753339675528L;
	private static final Logger logger = LoggerFactory
			.getLogger(IWMIDetailViewServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public IWMIDetailViewServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}

	private void doProcess(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			logger.debug("Water Data Detail view request came to process");

			String contentType = request.getParameter("ct");

			IWMISolrRequestHandler handler = createHandler();
			String solrQuery = handler.createQuery(contentType, null, null, 0, 1,
					null);
			QueryResponse queryResponse = handler.querySolr(solrQuery);
			SolrDocumentList docList = handler.getDocuments(queryResponse);

			if (null != docList && docList.size() > 0) {

				Map<String, FieldDetail> dataMap = getDataMap(docList
						.iterator().next());
				request.setAttribute(DOC_DATA.getDesc(), dataMap);
				request.setAttribute(FIELD_ORDER.getDesc(), getFieldOrder());
				logger.debug("Data Map loaded to request, {}", dataMap);
			} else {
				// no records found for this query.
				logger.info("No records found.");
				request.setAttribute(IWMIConst.NO_DATA.getDesc(),
						"No Records Found");
			}

			RequestDispatcher dispather = request
					.getRequestDispatcher("./jsp/projectDetailView.jsp");
			dispather.forward(request, response);
		} catch (Exception e) {
			logger.error("Error encountered while processing the request,", e);
			response.sendRedirect("index.jsp");
		}
	}

	@SuppressWarnings("unchecked")
	private Map<String, FieldDetail> getDataMap(SolrDocument doc) {
		Map<String, FieldDetail> dataMap = new HashMap<String, FieldDetail>(doc
				.getFieldNames().size());
		for (String fieldName : doc.getFieldNames()) {
			Object value = doc.getFieldValue(fieldName);
			String fieldValue = null;

			if (null == value)
				continue;
			logger.debug(
					"Field Detail -- > [ name :{}, type :{}, value :{}]",
					new Object[] { fieldName, value.getClass().getName(), value });

			if (value instanceof String) {
				String fldValue = (String) value;
				fldValue = (fldValue.startsWith(",")) ? fldValue.substring(1)
						: fldValue;
				if (isNullOrEmpty(fldValue))
					continue;
				else
					fieldValue = fldValue;
			} else if (value instanceof Date) {
				fieldValue = getFormattedDate((Date) value);
			} else if (value instanceof List) {
				List<String> datalist = (List<String>) value;
				StringBuilder sbuffer = new StringBuilder();

				for (String listVal : datalist) {
					if (!isNullOrEmpty(listVal)) {
						sbuffer.append(listVal).append(", ");
					}
				}
				fieldValue = sbuffer.toString();
			}
			FieldDetail field = FieldDetail.createFieldDetail(fieldName,
					fieldValue);
			if (null != field)
				dataMap.put(fieldName, field);
		}
		return dataMap;
	}

}
