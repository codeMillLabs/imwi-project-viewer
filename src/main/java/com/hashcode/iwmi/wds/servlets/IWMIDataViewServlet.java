package com.hashcode.iwmi.wds.servlets;

import static com.hashcode.iwmi.wds.utils.ResourceType.ALL;
import static com.hashcode.iwmi.wds.utils.ResourceType.AUDIO;
import static com.hashcode.iwmi.wds.utils.ResourceType.E_PROJECT;
import static com.hashcode.iwmi.wds.utils.ResourceType.MAPS;
import static com.hashcode.iwmi.wds.utils.ResourceType.MEDIA;
import static com.hashcode.iwmi.wds.utils.ResourceType.PHOTOS;
import static com.hashcode.iwmi.wds.utils.ResourceType.POSTERS;
import static com.hashcode.iwmi.wds.utils.ResourceType.POWER_POINTS;
import static com.hashcode.iwmi.wds.utils.ResourceType.SUCCESS_STORIES;
import static com.hashcode.iwmi.wds.utils.ResourceType.VIDEO;
import static com.hashcode.iwmi.wds.utils.ResourceType.WATER_DATA;
import static com.hashcode.iwmi.wds.utils.ResourceType.getType;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.iwmi.wds.service.AbstractViewHandler;
import com.hashcode.iwmi.wds.service.DataGridViewHandler;
import com.hashcode.iwmi.wds.service.DataSimpleListViewHandler;
import com.hashcode.iwmi.wds.utils.IWMIConst;
import com.hashcode.iwmi.wds.utils.ResourceType;

/**
 * <p>
 * Servlet to serve all the project data view related requests.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 * 
 */
public class IWMIDataViewServlet extends HttpServlet {

	private static final long serialVersionUID = 6625014672766987259L;
	private static final Logger logger = LoggerFactory
			.getLogger(IWMIDataViewServlet.class);

	private AbstractViewHandler dataViewHandler;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public IWMIDataViewServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}

	private void doProcess(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			logger.debug("Project view request came to process");

			String searchType = request.getParameter(IWMIConst.SEARCH_TYPE
					.getDesc());
			ResourceType resourceType = getType(searchType);

			switch (resourceType) {
			case WATER_DATA:
				dataViewHandler = new DataSimpleListViewHandler(WATER_DATA);
				break;
			case E_PROJECT:
				dataViewHandler = new DataSimpleListViewHandler(E_PROJECT);
				break;
			case AUDIO:
				dataViewHandler = new DataSimpleListViewHandler(AUDIO);
				break;
			case MEDIA:
				dataViewHandler = new DataSimpleListViewHandler(MEDIA);
				break;
			case POSTERS:
				dataViewHandler = new DataSimpleListViewHandler(POSTERS);
				break;
			case POWER_POINTS:
				dataViewHandler = new DataSimpleListViewHandler(POWER_POINTS);
				break;
			case SUCCESS_STORIES:
				dataViewHandler = new DataSimpleListViewHandler(SUCCESS_STORIES);
				break;
			case VIDEO:
				dataViewHandler = new DataSimpleListViewHandler(VIDEO);
				break;
			case MAPS:
				dataViewHandler = new DataGridViewHandler(MAPS);
				break;
			case PHOTOS:
				dataViewHandler = new DataGridViewHandler(PHOTOS);
				break;
			case ALL:
			default:
				dataViewHandler = new DataGridViewHandler(ALL);
				break;
			}

			if (null != dataViewHandler)
				dataViewHandler.handleRequest(request, response);

		} catch (Throwable e) {
			logger.debug("Error encountered while processing the request,", e);
			logger.error("Error encountered while processing the request, {}", e.getMessage());
			response.sendRedirect("index.jsp");
		}
	}

}
